<?php

namespace OpenSourceDeveloper\SamplePlugin\Request\Controllers\Api;

use Illuminate\Http\Request;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Controller;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\RoutingService;

/**
 * Class HomeController
 *
 * @package OpenSourceDeveloper\SamplePlugin\Request\Controllers\Api
 */
class HomeController extends Controller
{

    public function index(RoutingService $routingService)
    {
        return $routingService->renderJsonSuccessResponse(200, 'test plugin loaded');
    }

}
